﻿using UnityEngine;
using System.Collections.Generic;

public class PoolManager : MonoBehaviour
{
    public string poolName;
    public GameObject objectPrefab;
    public int startingPoolSize;
    public bool willGrow = true;
    public List<GameObject> objectPool;

    void Awake()
    {
        objectPool = new List<GameObject>();
        CreateObjects();
    }

    void CreateObjects()
    {
        for (int i = 0; i < startingPoolSize; i++)
        {
            GameObject createdObject = Instantiate(objectPrefab);
            createdObject.transform.parent = gameObject.transform;
            createdObject.SetActive(false);
            createdObject.name = poolName + i;
            objectPool.Add(createdObject);
        }
    }

    public GameObject GetPooledObject()
    {
        for (int i = 0; i < objectPool.Count; i++)
        {
            if (!objectPool[i].activeInHierarchy)
            {
                objectPool[i].SetActive(true);
                return objectPool[i];
            }
        }

        if (willGrow)
        {
            GameObject newObject = Instantiate(objectPrefab);
            objectPool.Add(newObject);
            newObject.transform.parent = gameObject.transform;
            newObject.name = poolName + objectPool.IndexOf(newObject);
            return newObject;
        }

        return null;

    }

    public void ReturnToPool(GameObject objectToReturn)
    {
        objectToReturn.SetActive(false);   
    }
	
}
