﻿using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class TreasureChest : MonoBehaviour 
{
	public LayerMask InteractiveItems;

	public GameObject CoinPrefab;

	public Transform CoinSpawnPosition;

	public Text CoinCounterTextField;

	Vector3 _tweenTargetPosition;

	int _score;

    PoolManager _poolManager;
    RectTransform coinCounterRect;
    Vector3[] coinCounterCorners;

    private const float tweenDuration = 0.5f;

	void Start()
	{
        _poolManager = GetComponent<PoolManager>();

        coinCounterRect = CoinCounterTextField.rectTransform;
        coinCounterCorners = new Vector3[4];

    }

    void Update ()
    {
        if ( Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
            if (Physics.Raycast (ray, out hit, 100f, InteractiveItems)) 
            {
                SpawnCoin ();
            }
        }
    }

    void SpawnCoin ()
    {
        coinCounterRect.GetWorldCorners(coinCounterCorners);
        Vector2 bottomLeft = coinCounterRect.TransformPoint(coinCounterRect.rect.min);
        _tweenTargetPosition = Camera.main.ScreenToWorldPoint(new Vector3(bottomLeft.x, bottomLeft.y, Camera.main.nearClipPlane));


        GameObject coin = _poolManager.GetPooledObject();
        coin.transform.position = CoinSpawnPosition.position;
        coin.transform.localScale = Vector3.one;

        DOTween.To (() => coin.transform.position, x => coin.transform.position = x, _tweenTargetPosition, tweenDuration).OnComplete (()=>OnCoinTweenComplete(coin));
        DOTween.To (() => coin.transform.localScale, x => coin.transform.localScale = x,Vector3.zero, tweenDuration);
	}

    void OnCoinTweenComplete (GameObject coin)
	{
        _poolManager.ReturnToPool(coin);

		_score++;

		CoinCounterTextField.text = "Score: " + _score;
	}
}
