﻿using UnityEngine;
using UnityEngine.UI;

public class PauseDemo : MonoBehaviour 
{
    public Image PauseTint;

	private float defaultTimeScale;

	void Start()
	{
		defaultTimeScale = Time.timeScale;
        Unpause();
	}

    void OnDestroy()
    {
        Time.timeScale = defaultTimeScale;
    }

	public void TogglePause()
	{
		if (Time.timeScale == 0f) 
		{
            Unpause();
        }
        else
        {
            Pause();
        }
    }

    void Pause()
    {
        Time.timeScale = 0f;
        PauseTint.enabled = true;
    }

    void Unpause()
    {
        Time.timeScale = defaultTimeScale;
        PauseTint.enabled = false;
    }
}
