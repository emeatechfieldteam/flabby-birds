using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DropMe : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
{
	private Image dropBoxBackground;
    private Image imageDisplay;

    private Color normalColor;
	
	public void OnEnable ()
	{
        imageDisplay = GetComponent<Image>();
        dropBoxBackground = transform.parent.GetComponent<Image>();
        normalColor = dropBoxBackground.color;
	}
	
	public void OnDrop(PointerEventData data)
	{
		dropBoxBackground.color = normalColor;

        UpdateSprite(data);
    }

    void UpdateSprite(PointerEventData data)
    {
        Sprite spriteBeingDropped = GetSpriteBeingDropped (data);
        
        if (spriteBeingDropped != null)
            imageDisplay.overrideSprite = spriteBeingDropped;
    }

	public void OnPointerEnter(PointerEventData data)
	{
        dropBoxBackground.color = Color.yellow;
	}

	public void OnPointerExit(PointerEventData data)
	{
		dropBoxBackground.color = normalColor;
	}
	
	private Sprite GetSpriteBeingDropped(PointerEventData data)
	{
        if (data.pointerDrag == null)
            return null;
        
        Image imageBeingDragged = data.pointerDrag.GetComponent<Image>();
		
		return imageBeingDragged.sprite;
	}
}
