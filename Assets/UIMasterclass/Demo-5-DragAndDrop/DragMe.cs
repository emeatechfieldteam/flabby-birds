using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class DragMe : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public Canvas canvas;

    private GameObject dragIconInstance;

    RectTransform dragIconRect;

	public void OnBeginDrag(PointerEventData eventData)
	{
        if (dragIconInstance == null)
        {
            GenerateDragIconInstance();
        }
        else
        {
            dragIconInstance.SetActive(true);
        }

        dragIconRect.position = eventData.position;
	}

    void GenerateDragIconInstance()
    {
        dragIconInstance = new GameObject(name + "_dragIcon");

        dragIconInstance.transform.SetParent (canvas.transform, false);

        Image image = dragIconInstance.AddComponent<Image>();
        image.sprite = GetComponent<Image>().sprite;
        image.SetNativeSize();

        CanvasGroup group = dragIconInstance.AddComponent<CanvasGroup>();
        group.blocksRaycasts = false;

        dragIconRect = dragIconInstance.GetComponent<RectTransform>();
    }

	public void OnDrag(PointerEventData eventData)
	{
        dragIconRect.position = eventData.position;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        dragIconInstance.SetActive(false);
    }
}
