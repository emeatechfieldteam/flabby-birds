﻿using UnityEngine;
using UnityEngine.UI;

public class Healthbar : MonoBehaviour
{
    public Text nameText;

    public Image HealthbarFillImage;

    public void SetName(string name)
    {
        nameText.text = name;
    }

    public void SetHealthbarFillAmount(float amount)
    {
        HealthbarFillImage.fillAmount = amount;

        if (amount > 0.66f)
        {
            HealthbarFillImage.color = Color.green;
        }
        else if (amount > 0.33f)
        {
            HealthbarFillImage.color = Color.yellow;
        }
        else
        {
            HealthbarFillImage.color = Color.red;
        }
    }

    void Update()
    {
        transform.LookAt(Camera.main.transform.position);
    }
}
