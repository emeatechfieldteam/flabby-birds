﻿using UnityEngine;
using System.Collections.Generic;

public class RTSCharacter : MonoBehaviour 
{
    private const float maxHealth = 1;
    private float currentHealth;

    public Transform HealthBarHolder;

    public GameObject HealthBarPrefab;

    public Healthbar healthbar;

    public string Name;

    private NavMeshAgent _agent;

    public List<Transform> wanderWaypoints;

    private Vector3 _currentTargetPosition;

    void Awake()
    {
        currentHealth = maxHealth;

        GameObject healthbarInstance = Instantiate(HealthBarPrefab);
        healthbar = healthbarInstance.GetComponent < Healthbar>();

        healthbarInstance.transform.SetParent(HealthBarHolder);
        healthbarInstance.transform.localPosition = Vector3.zero;

        healthbar.SetName(Name);
        healthbar.SetHealthbarFillAmount(1f);

        _agent = GetComponent<NavMeshAgent>();


    }

    void OnEnable () 
    {
        SetRandomTargetWaypoint();
    }

    void OnMouseUp() 
    {
        currentHealth -= 0.1f;
        healthbar.SetHealthbarFillAmount(currentHealth);

        if (currentHealth <= 0)
        {
            Destroy(gameObject);
        }
    }

    void SetRandomTargetWaypoint()
    {
        int random = Random.Range(0, wanderWaypoints.Count - 1);
        _currentTargetPosition = wanderWaypoints[random].position;
        _agent.SetDestination(_currentTargetPosition);
    }

    // Update is called once per frame
    void Update () {
        if (Vector3.Distance(transform.position, _currentTargetPosition) < 1f)
        {
            SetRandomTargetWaypoint();
        }
    }
}
