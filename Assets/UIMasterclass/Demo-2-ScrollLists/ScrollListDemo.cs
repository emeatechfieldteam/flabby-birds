﻿using UnityEngine;

public class ScrollListDemo : MonoBehaviour 
{
    public Transform ButtonLayoutGroup;
    public RectTransform ButtonPrefab;

    public void AddItemToScrollList()
    {
        RectTransform InstantiatedButton = Instantiate(ButtonPrefab);
        InstantiatedButton.transform.SetParent(ButtonLayoutGroup);
    }
}
