﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ColourPickerDemo : MonoBehaviour 
{
    public RawImage rawColourwheelimage;
    public RectTransform ColourWheelRect;

    public SkinnedMeshRenderer RendererToColour;

    public void ColourPickerDragCallback(BaseEventData eventData)
    {
        PointerEventData ped = eventData as PointerEventData;

        Vector2 coords = ped.pointerDrag.gameObject.transform.InverseTransformPoint(ped.position);
        Texture2D tex = rawColourwheelimage.texture as Texture2D;
        Color c = tex.GetPixel((int)coords.x + 128, (int)coords.y + 128);
      
        RendererToColour.material.color = c;
    }
}
