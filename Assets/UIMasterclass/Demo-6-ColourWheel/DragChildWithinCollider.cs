﻿using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent (typeof (RectTransform), typeof (Collider2D))]
public class DragChildWithinCollider : MonoBehaviour, IDragHandler, ICanvasRaycastFilter, IPointerEnterHandler, IPointerExitHandler
{
    public RectTransform ChildToDrag;

    private bool childFollowingMouse;

    private Collider2D maskCollider;
    private RectTransform rectTransform;

    public EventTrigger.TriggerEvent DragCallback;

    void Start()
    {
        maskCollider = GetComponent<Collider2D>();
        rectTransform = GetComponent<RectTransform>();
    }

    public bool IsRaycastLocationValid (Vector2 screenPos, Camera eventCamera)
    {
        Vector3 worldPoint = Vector3.zero;
        bool isInside = RectTransformUtility.ScreenPointToWorldPointInRectangle(
            rectTransform,
            screenPos,
            eventCamera,
            out worldPoint
        );
        if (isInside)
            isInside = maskCollider.OverlapPoint(worldPoint);

        return isInside;
    }


    public void OnPointerEnter(PointerEventData eventData)
    {
        childFollowingMouse = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        childFollowingMouse = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (childFollowingMouse)
        {
            SetDraggedPosition(eventData);

            if (DragCallback != null)
            {
                DragCallback.Invoke(eventData);
            }
        }
    }

    void SetDraggedPosition(PointerEventData eventData)
    {
        Vector3 globalMousePos;
        if (RectTransformUtility.ScreenPointToWorldPointInRectangle(rectTransform, eventData.position, eventData.pressEventCamera, out globalMousePos))
        {
            ChildToDrag.position = globalMousePos;
        }
    }
}
