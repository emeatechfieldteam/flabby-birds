﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuDropdown : MonoBehaviour 
{
	private Dropdown _sceneSelectDropdown;

	void Start()
	{
		_sceneSelectDropdown = GetComponent<Dropdown> ();
	}

	public void LoadScene()
	{
		SceneManager.LoadScene (_sceneSelectDropdown.value);
	}
}
