﻿using UnityEngine;

public class Rotate : MonoBehaviour 
{
    public Vector3 rotationAmount;

	void Update () 
    {
        transform.Rotate(rotationAmount * Time.deltaTime);
	}
}
