﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ProgressBarDemo : MonoBehaviour 
{
	public Image StandardProgressBarFillImage;
    public Image AnimatedProgressBarFillImage;
    public Image RotatingProgressBarFillImage;

	private const float _amountToFillPerClick = 0.25f;
    private float progress = 0;

    void Start()
    {
        UpdateProgressDisplay();
    }

	public void AddToProgressBar()
	{
        if (StandardProgressBarFillImage.fillAmount >= 1)
        {
            progress = 0f;
        }
        else
        {
            progress += _amountToFillPerClick;
        }

        UpdateProgressDisplay();
	}

    void UpdateProgressDisplay()
    {
        StandardProgressBarFillImage.fillAmount = progress;
        AnimatedProgressBarFillImage.fillAmount = progress;
        RotatingProgressBarFillImage.fillAmount = progress;
    }
}
